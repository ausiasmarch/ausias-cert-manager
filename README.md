# ausias-certificate-manager
(c) 2020 Luis Garcia Gisbert
## Description
This project implements a simple CA and x509 certificate management system based on easyRSA.
Despite of the 'ausias' prefix name, there is nothing specific to my organization in the code, just
the sample values for COUNTRY, CITY, etc ... in the default vars file, so the project can be easily
adapted and used in any environment.

The package installs a simple script (ausias-cm) capable of:

- Create custom private CAs

- Create multiple certificate/key pairs for servers and clients.
  The certificates can be used for VPNs or web services.

In order to personalize a CA, a "configuration template file" (just an easyRSA 'vars' file) can be provided BEFORE the CA creation.
The configuration file full path name must be;

'/etc/ausias-cert-manager/CA_COMMON_NAME.vars'.

If this file is not present when creating a CA, the script searchs for a file named 'default.vars' in the same directory.
The debian package installs the file 'install.cm/usr/share/ausias-cert-manager/default/vars.template' as default configuration.

Please feel free to customize this file to support your own needs.

A directory under /etc/ssl/ausias-cert-manager is created for each CA. The generated certificates and keys are stored in folder 'keys'

Run the 'ausias-cm' script without parameters for help, or ... "Use the Source, Luke" for further progress ...

## Debian packaging quick start
Clone the git repository and install build-essential and debhelper packages with apt. 'cd' to project's directory and run:

dpkg-buildpackage -rfakeroot -uc -us -sa -i.git

After package construction, a ".deb" file must be found in the parent directory. Install openssl, easy-rsa (and openvpn if needed) before executing:

dpkg -i ../PACKAGE.deb

## License: GPL-3+

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along 
with this program; if not, write to the Free Software Foundation, Inc., 
51 Franklin St, Fifth Floor, Boston MA 02110-1301 USA


